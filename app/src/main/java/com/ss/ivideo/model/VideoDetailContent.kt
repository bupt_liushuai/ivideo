package com.ss.ivideo.model

/**
 * Created by liushuai on 2019-11-07
 */
data class VideoDetailContent(var playListItem: List<VideoPlayListItem>? = null,
                              var title: String? = null,
                              var subTitle: String? = null,
                              var desc: String? = null,
                              var recommend: List<VideoItemModel>? = null,
                              var sensitive: Boolean? = false)

data class VideoPlayListItem(val name: String, var url: String?, var isPlay: String? = null)
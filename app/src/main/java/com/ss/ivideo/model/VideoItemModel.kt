package com.ss.ivideo.model

/**
 * Created by liushuai on 2019-11-01
 */
class VideoItemModel(var title: String = "",
                     var thumb: String = "",
                     var url: String = "",
                     var range: String = "",
                     var note:String = "")
package com.ss.ivideo.model

/**
 * Created by liushuai on 2019-11-22
 */
data class VideoCategoryModel(var itemModels: List<VideoItemModel>? = null,
                              var category: Map<Int, List<Pair<String, String>>>? = null)
package com.ss.ivideo.model

/**
 * Created by liushuai on 2019-11-05
 */
class VideoSectionData {
    var title: String = ""
    var list = mutableListOf<VideoItemModel>()
}
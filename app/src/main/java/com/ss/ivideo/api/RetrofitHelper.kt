package com.ss.ivideo.api

import com.ss.ivideo.constant.Constant
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.security.cert.X509Certificate
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager


/**
 * Created by liushuai on 2019-11-02
 */
object RetrofitHelper {

    private var retrofit: Retrofit? = null

    private fun getRetrofit(): Retrofit {
        if (retrofit == null) {
            val okhttpClient = OkHttpClient().newBuilder()
            //信任所有服务器地址
            okhttpClient.hostnameVerifier { s, sslSession ->
                //设置为true
                true
            }
            //创建管理器
            val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
                override fun getAcceptedIssuers(): Array<X509Certificate> {
                    return arrayOf()
                }

                @Throws(java.security.cert.CertificateException::class)
                override fun checkClientTrusted(
                    x509Certificates: Array<java.security.cert.X509Certificate>,
                    s: String
                ) {
                }

                @Throws(java.security.cert.CertificateException::class)
                override fun checkServerTrusted(
                    x509Certificates: Array<java.security.cert.X509Certificate>,
                    s: String
                ) {
                }
            })
            try {
                val sslContext = SSLContext.getInstance("TLS")
                sslContext.init(null, trustAllCerts, java.security.SecureRandom())

                //为OkHttpClient设置sslSocketFactory
                okhttpClient.sslSocketFactory(sslContext.socketFactory)

            } catch (e: Exception) {
                e.printStackTrace()
            }


            retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okhttpClient.build())
                .baseUrl(Constant.URL_BASE).build()

        }
        return retrofit!!
    }

    fun <T> getService(t: Class<T>): T {
        return getRetrofit().create(t)
    }
}
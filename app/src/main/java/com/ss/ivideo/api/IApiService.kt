package com.ss.ivideo.api

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Url


/**
 * Created by liushuai on 2019-11-02
 */

interface IApiService {
    @GET
    fun getHtml(@Url url: String): Call<ResponseBody>
}
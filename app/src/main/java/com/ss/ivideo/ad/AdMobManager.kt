package com.ss.ivideo.ad

import android.app.Activity
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.View
import android.view.ViewGroup
import com.google.android.ads.nativetemplates.NativeTemplateStyle
import com.google.android.ads.nativetemplates.TemplateView
import com.google.android.gms.ads.*
import com.google.android.gms.ads.formats.UnifiedNativeAd
import com.google.android.gms.ads.rewarded.RewardItem
import com.google.android.gms.ads.rewarded.RewardedAd
import com.google.android.gms.ads.rewarded.RewardedAdCallback
import com.ss.ivideo.constant.Constant
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback
import com.ss.ivideo.utils.SharedPreferencesUtils


/**
 * Created by Liushuai on 2019-12-01.
 */
object AdMobManager {
    const val TAG = "AdMobManager"

    private fun getAdRequestBuilder(): AdRequest {
        return AdRequest.Builder()
//            .addTestDevice("32C17E2D0E965064850B0ADB9CF274AC")
            .build()
    }

    fun addBanner(container: ViewGroup, id: String) {
        MobileAds.initialize(container.context) {}
        val adView = AdView(container.context)
        container.addView(
            adView,
            ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
        )
        val adRequest = getAdRequestBuilder()
        adView.adSize = AdSize(360, 50)
        adView.adUnitId = id
        adView.loadAd(adRequest)
        adView.adListener = object : AdListener() {
            override fun onAdFailedToLoad(p0: Int) {
                super.onAdFailedToLoad(p0)
                Log.e(TAG, "Banner onAdFailedToLoad $p0")
            }

            override fun onAdLoaded() {
                super.onAdLoaded()
                Log.e(TAG, "Banner onAdLoaded")
            }
        }
    }


    fun addSplashInterstitialAd(template: TemplateView, callback: (Boolean) -> Unit) {
        MobileAds.initialize(template.context) {}
        val context = template.context
        template.visibility = View.INVISIBLE
        val adLoader = AdLoader.Builder(template.context, Constant.APP_SPLASH_AD_ID)
            .forUnifiedNativeAd {
                val styles = NativeTemplateStyle.Builder()
                    .withMainBackgroundColor(ColorDrawable(context.resources.getColor(android.R.color.white)))
                    .build()
                template.setStyles(styles)
                template.setNativeAd(it)
            }
            .withAdListener(object : AdListener() {
                override fun onAdFailedToLoad(p0: Int) {
                    super.onAdFailedToLoad(p0)
                    callback(false)
                    Log.e(TAG, "addSplashInterstitialAd onAdFailedToLoad")
                }

                override fun onAdLoaded() {
                    super.onAdLoaded()
                    template.visibility = View.VISIBLE
                    callback(true)
                    Log.e(TAG, "addSplashInterstitialAd onAdLoaded")

                }
            })

            .build()

        adLoader.loadAd(getAdRequestBuilder())
    }

    fun addRewardedAd(activity: Activity, callback: (Boolean, RewardedAd) -> Unit) {

        val rewardedAd = RewardedAd(activity, Constant.APP_REWARD_AD_ID)
        rewardedAd.loadAd(getAdRequestBuilder(), object : RewardedAdLoadCallback() {
            override fun onRewardedAdLoaded() {
                // Ad successfully loaded.
                Log.e(TAG, "RewardedAd onAdLoaded")
                callback(true, rewardedAd)
//
            }

            override fun onRewardedAdFailedToLoad(errorCode: Int) {
                // Ad failed to load.
                Log.e(TAG, "RewardedAd onAdFailed")
                callback(false, rewardedAd)
            }
        })
    }

    fun showRewardAd(activity: Activity, rewardedAd: RewardedAd, callback: (Boolean, Int) -> Unit) {
        rewardedAd.show(activity, object : RewardedAdCallback() {
            override fun onUserEarnedReward(p0: RewardItem) {
                callback(p0.amount > 0, p0.amount)
            }

            override fun onRewardedAdClosed() {
                super.onRewardedAdClosed()
                callback(false, 0)
            }
        }, true)
    }

}
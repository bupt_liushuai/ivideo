package com.ss.ivideo.cagegory

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.ss.ivideo.R
import com.ss.ivideo.constant.Constant
import com.ss.ivideo.core.HtmlDataLoader
import com.ss.ivideo.core.HtmlParseUtils
import com.ss.ivideo.detail.DetailActivity
import com.ss.ivideo.model.VideoCategoryModel
import com.ss.ivideo.model.VideoItemModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_category.*
import kotlinx.android.synthetic.main.fragment_home.recycler_view

/**
 * Created by liushuai on 2019-10-30
 */

open abstract class CategoryBaseListFragment : Fragment() {

    var adapter: VideoListAdapter? = null
    var page = 0
    var disposable: Disposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onDestroy() {
        if (disposable?.isDisposed == false) {
            disposable?.dispose()
        }
        super.onDestroy()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_category, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun initView() {
        adapter = VideoListAdapter().apply {
            setOnItemClickListener { adapter, view, position ->
                val cxt = context
                cxt ?: return@setOnItemClickListener
                adapter.data.getOrNull(position)?.let { it ->
                    (it as? VideoItemModel)?.let { item ->
                        DetailActivity.open(cxt, item.url, item.title)
                    }
                }

            }
        }
        recycler_view.adapter = adapter
        recycler_view.layoutManager = StaggeredGridLayoutManager(2, RecyclerView.VERTICAL)
        adapter?.setEnableLoadMore(true)
        adapter?.setOnLoadMoreListener {
            loadData(false)
        }

        swipelayout.setOnRefreshListener {
            loadData(true)
        }
    }

    fun loadData(isRefresh: Boolean) {
        if (isRefresh) {
            swipelayout.isRefreshing = true
        }
        val url = generateUrl(isRefresh)
        println("url ======= $url")
        disposable = HtmlDataLoader.getCategoryListAsync(url)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    loadSuccess(isRefresh, it)
                }, {
                    loadFailed(isRefresh)
                })
    }

    abstract fun generateUrl(isRefresh: Boolean): String

    open fun loadSuccess(isRefresh: Boolean, videoCategoryModel: VideoCategoryModel) {
        val list = videoCategoryModel.itemModels?: mutableListOf()
        if (isRefresh) {
            swipelayout.isRefreshing = false
        }
        page++
        if (isRefresh) {
            adapter?.setNewData(list)
        } else {
            adapter?.addData(list)
            if (list.isEmpty()) {
                adapter?.loadMoreEnd()
            } else {
                adapter?.loadMoreComplete()
            }
        }

    }

    private fun loadFailed(isRefresh: Boolean) {
        if (isRefresh) {
            swipelayout.isRefreshing = false
        } else {
            adapter?.loadMoreFail()
        }
    }
}
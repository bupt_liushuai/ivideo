package com.ss.ivideo.cagegory

import android.widget.ImageView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.ss.ivideo.R
import com.ss.ivideo.core.GlideUtils
import com.ss.ivideo.model.VideoItemModel

/**
 * Created by liushuai on 2019-11-01
 */
class VideoListAdapter :
    BaseQuickAdapter<VideoItemModel, BaseViewHolder>(R.layout.item_video_list) {
    override fun convert(helper: BaseViewHolder, item: VideoItemModel?) {
        item ?: return
        helper.getView<ImageView>(R.id.img).apply {
            GlideUtils.loadImage(this, item.thumb, 5)
        }
        helper.setText(R.id.title, item.title)
        helper.setText(R.id.note, item.note)
    }
}
package com.ss.ivideo.cagegory

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import com.ss.ivideo.R
import com.ss.ivideo.model.VideoCategoryModel
import com.ss.ivideo.view.CategorySelector

/**
 * Created by liushuai on 2019-10-30
 */

class CategoryListFragment : CategoryBaseListFragment() {

    var url = ""
    var classify = ""
    var region = ""
    var time = ""
    var hot = ""

    var category: Map<Int, List<Pair<String, String>>>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.apply {
            url = getString(CategoryListActivity.PARAM_URL, "")
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadData(true)
    }

    override fun generateUrl(isRefresh: Boolean): String {
        val page = if (isRefresh) 0 else {
            page
        }
        return String.format(url, args = *arrayOf(classify, region, time, page, hot))
    }


    override fun loadSuccess(isRefresh: Boolean, videoCategoryModel: VideoCategoryModel) {
        super.loadSuccess(isRefresh, videoCategoryModel)
        if (isRefresh && category == null) {
            bindVideoCategory(videoCategoryModel.category)
        }
    }

    private fun bindVideoCategory(category: Map<Int, List<Pair<String, String>>>? = null) {

        val header = LayoutInflater.from(context).inflate(R.layout.item_category_header, null) as CategorySelector
        adapter?.removeAllHeaderView()
        adapter?.addHeaderView(header)

        this.category = category
        val list = mutableListOf<List<Pair<String, String>>>()
        val size = category?.keys?.size?: 0
        if (size > 0) {
            for (i in 0 until size) {
                category?.get(i)?.let {
                    list.add(it)
                }
            }
        }
        header.list = list
        header.setCallback { s0, s1, s2, s3 ->
            classify = s0
            region = s1
            time = s2
            hot = s3
            loadData(true)
        }

//
//
//        list.add()
//        header.setCategoryData(list)
    }


}
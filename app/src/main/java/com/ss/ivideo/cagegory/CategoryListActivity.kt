package com.ss.ivideo.cagegory

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import com.ss.ivideo.R
import com.ss.ivideo.base.BaseActivity
import com.ss.ivideo.detail.DetailActivity
import com.ss.ivideo.search.SearchActivity

/**
 * Created by liushuai on 2019-11-05
 */
class CategoryListActivity : BaseActivity() {

    companion object {

        const val PARAM_URL = "url"
        const val PARAM_TITLE = "title"
        fun open(context: Context?, url: String?, title: String) {
            url ?: return
            context ?: return
            context.startActivity(Intent(context, CategoryListActivity::class.java).apply {
                putExtra(PARAM_URL, url)
                putExtra(PARAM_TITLE, title)
            })
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_category)

        val f = CategoryListFragment().apply {
            arguments = intent.extras
        }
        supportFragmentManager.beginTransaction().replace(R.id.fragment_container, f).commit()

        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            val title = intent.getStringExtra(PARAM_TITLE)
            if (!TextUtils.isEmpty(title)) {
                setTitle(title)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.itemId == R.id.search) {
            startActivity(Intent(this, SearchActivity::class.java))
        } else if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
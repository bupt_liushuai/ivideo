package com.ss.ivideo.event

/**
 * Created by liushuai on 2019-11-01
 */
data class ItemClickEvent(val position: Int)
package com.ss.ivideo.event

/**
 * Created by liushuai on 2019-11-01
 */
data class LoadHtmlEvent(val url: String, val interceptM3u8: Boolean = false)
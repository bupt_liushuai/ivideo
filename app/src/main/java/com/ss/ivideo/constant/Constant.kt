package com.ss.ivideo.constant

/**
 * Created by liushuai on 2019-11-01
 */
object Constant {
    const val URL_BASE = "https://www.dandanzan.com"
    const val URL_DY = "${URL_BASE}/dianying/%s-%s-%s-%d-%s.html"
    const val URL_DSJ = "${URL_BASE}/dianshiju/%s-%s-%s-%d-%s.html"
    const val URL_ZY = "${URL_BASE}/zongyi/%s-%s-%s-%d-%s.html"
    const val URL_DM = "${URL_BASE}/dongman/%s-%s-%s-%d-%s.html"
    const val URL_SEARCH = "${URL_BASE}/so/%s-%s-%d-%s.html"


    //admob ad
    const val APP_DETAIL_BANNER_ID = "ca-app-pub-5996491245726222/1088355196"
//    const val APP_DETAIL_BANNER_ID = "ca-app-pub-3940256099942544/6300978111"
    const val APP_SEARCH_BANNER_ID = "ca-app-pub-5996491245726222/3650815642"

    const val APP_SPLASH_AD_ID = "ca-app-pub-5996491245726222/5283585223"
//    const val APP_SPLASH_AD_ID = "ca-app-pub-3940256099942544/2247696110"

    const val APP_REWARD_AD_ID = "ca-app-pub-5996491245726222/2665569860"
}
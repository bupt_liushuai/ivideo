package com.ss.ivideo.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ss.ivideo.R
import com.ss.ivideo.cagegory.CategoryBaseListFragment
import com.ss.ivideo.constant.Constant
import com.ss.ivideo.model.VideoCategoryModel
import com.ss.ivideo.view.CategorySelector

/**
 * Created by Liushuai on 2019/11/17.
 */
class SearchFragment: CategoryBaseListFragment() {
    var category: Map<Int, List<Pair<String, String>>>? = null
    var hot = "newstime"

    var keyWord: String = ""
        set(value) {
            field = value
            adapter?.setNewData(mutableListOf())
            loadData(true)
        }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }


    override fun generateUrl(isRefresh: Boolean): String {
        val page = if (isRefresh) 0 else {
            page
        }
        return String.format(Constant.URL_SEARCH, args = *arrayOf(keyWord, keyWord, page, hot))
    }

    override fun loadSuccess(isRefresh: Boolean, videoCategoryModel: VideoCategoryModel) {
        super.loadSuccess(isRefresh, videoCategoryModel)
        if (isRefresh && category == null) {
            bindVideoCategory(videoCategoryModel.category)
        }
    }


    private fun bindVideoCategory(category: Map<Int, List<Pair<String, String>>>? = null) {

        val header = LayoutInflater.from(context).inflate(
            R.layout.item_category_header,
            null
        ) as CategorySelector
        adapter?.removeAllHeaderView()
        adapter?.addHeaderView(header)

        this.category = category

        header.list = mutableListOf()
        header.setCallback { s0, s1, s2, s3 ->
            hot = s0
            loadData(true)
        }
    }
}
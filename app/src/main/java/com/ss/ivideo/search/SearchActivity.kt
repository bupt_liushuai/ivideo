package com.ss.ivideo.search

import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import com.jaeger.library.StatusBarUtil
import com.ss.ivideo.R
import com.ss.ivideo.ad.AdMobManager
import com.ss.ivideo.base.BaseActivity
import com.ss.ivideo.constant.Constant
import com.ss.ivideo.utils.GsonUtil
import com.ss.ivideo.utils.SharedPreferencesUtils
import com.ss.ivideo.utils.Utils
import kotlinx.android.synthetic.main.activity_search.*

/**
 * Created by Liushuai on 2019/11/17.
 */
class SearchActivity : BaseActivity() {

    companion object {
        const val MAX_HISTORY = 15
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        initView()
        initHistory()
        initAd()
        StatusBarUtil.setColor(this, resources.getColor(R.color.actionbarColor))
    }

    private fun initView() {
        val f = SearchFragment()
        supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, f).commitAllowingStateLoss()

        back.setOnClickListener {
            onBackPressed()
        }
        search.setOnClickListener {
            search()
        }

        clear.setOnClickListener {
            edit_input.setText("")
            Utils.showInput(edit_input)
        }

        edit_input.postDelayed({
            edit_input.requestFocus()
            Utils.showInput(edit_input)
        }, 150)

        edit_input.setOnKeyListener { _, keyCode, _ ->
            if (keyCode == KeyEvent.KEYCODE_ENTER) {
                search()
            }
            false
        }

        edit_input.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0?.isEmpty() == true) {
                    search_history.visibility = View.VISIBLE
                    tag_group.visibility = View.VISIBLE
                    fragment_container.visibility = View.GONE
                    clear.visibility = View.GONE
                } else {
                    clear.visibility = View.VISIBLE
                }
            }

        })

        clear_history.setOnClickListener {
            SharedPreferencesUtils.searchHistory = ""
            tag_group.setTags(mutableListOf())
        }

        tag_group.setOnTagClickListener {
            edit_input.setText(it)
            try {
                edit_input.setSelection(it.length)
            } catch (e: Exception) {
            }
            search()
        }
    }

    private fun updateHistory(searchKey: String) {
        var list = getHistorySearchList()
        if (list.isNotEmpty()) {
            list.remove(searchKey)//去重？
        }
        if (list.size > MAX_HISTORY) {
            list = list.subList(0, MAX_HISTORY)
        }
        list.add(0, searchKey)
        SharedPreferencesUtils.searchHistory = GsonUtil.toJson(list)
        tag_group.setTags(list)
    }

    private fun getHistorySearchList(): MutableList<String> {
        var result = mutableListOf<String>()
        val historyStr = SharedPreferencesUtils.searchHistory
        if (!TextUtils.isEmpty(historyStr)) {
            result = GsonUtil.jsonToList(historyStr, String::class.java)
        }
        return result
    }

    private fun initHistory() {
        val list = getHistorySearchList()
        tag_group.setTags(list)
    }

    private fun initAd() {
        AdMobManager.addBanner(bannerContainer, Constant.APP_SEARCH_BANNER_ID)
    }

    private fun search() {
        if (edit_input.text.isNotBlank()) {
            val f = supportFragmentManager.findFragmentById(R.id.fragment_container) as SearchFragment
            search_history.visibility = View.GONE
            tag_group.visibility = View.GONE
            fragment_container.visibility = View.VISIBLE
            f.keyWord = edit_input.text.toString()
            Utils.hideInput(edit_input)
            updateHistory(edit_input.text.toString())
        }
    }


    override fun onBackPressed() {
        if (fragment_container.visibility == View.VISIBLE) {
            clear.performClick()
        } else {
            Utils.hideInput(edit_input)
            super.onBackPressed()
        }
    }
}
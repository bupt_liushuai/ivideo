package com.ss.ivideo.detail

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.jaeger.library.StatusBarUtil
import com.ss.ivideo.R
import com.ss.ivideo.base.BaseActivity
import com.ss.ivideo.cagegory.DetailFragment
import kotlinx.android.synthetic.main.fragment_detail.*

/**
 * Created by liushuai on 2019-11-02
 */
class DetailActivity : BaseActivity() {
    var f: DetailFragment? = null

    companion object {

        const val PARAM_URL = "url"
        const val PARAM_TITLE = "title"
        fun open(context: Context, url: String?, title: String? = "") {
            url ?: return
            context.startActivity(Intent(context, DetailActivity::class.java).apply {
                putExtra(PARAM_URL, url)
                putExtra(PARAM_TITLE, title)
            })
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(
            R.style.AppTheme_Custom
        )
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        setStatusBarColor(android.R.color.black)
        f = DetailFragment().apply {
            url = intent.getStringExtra(PARAM_URL)
            title = intent.getStringExtra(PARAM_TITLE)
            supportFragmentManager.beginTransaction().replace(R.id.fragment_container, this)
                .commit()
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        //如果旋转了就全屏
        video_player.onConfigurationChanged(
            this,
            newConfig,
            f?.orientationUtils,
            true,
            true
        )
    }

    override fun onBackPressed() {
        if (f?.onbackPress() == true) {
            return
        }
        super.onBackPressed()
    }
}
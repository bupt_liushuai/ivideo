package com.ss.ivideo.detail

import android.widget.TextView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.ss.ivideo.R
import com.ss.ivideo.model.VideoPlayListItem

/**
 * Created by liushuai on 2019-11-07
 */
class PlayListAdapter(layoutId: Int = R.layout.item_play_list):
    BaseQuickAdapter<VideoPlayListItem, BaseViewHolder>(layoutId) {
    override fun convert(helper: BaseViewHolder, item: VideoPlayListItem?) {
        item ?: return
        helper.setText(R.id.title, item.name)
        (helper.getView<TextView>(R.id.title)).isSelected = item.isPlay == "on"
    }
}
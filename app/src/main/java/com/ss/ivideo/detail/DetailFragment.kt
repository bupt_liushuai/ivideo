package com.ss.ivideo.cagegory

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.materialdialogs.MaterialDialog
import com.shuyu.gsyvideoplayer.builder.GSYVideoOptionBuilder
import com.shuyu.gsyvideoplayer.listener.GSYSampleCallBack
import com.shuyu.gsyvideoplayer.utils.OrientationUtils
import com.shuyu.gsyvideoplayer.video.GSYSampleADVideoPlayer
import com.ss.ivideo.R
import com.ss.ivideo.ad.AdMobManager
import com.ss.ivideo.constant.Constant
import com.ss.ivideo.core.HtmlParseUtils
import com.ss.ivideo.core.WebViewEngine
import com.ss.ivideo.detail.DetailActivity
import com.ss.ivideo.detail.DetailPlayListDialogFragment
import com.ss.ivideo.detail.PlayListAdapter
import com.ss.ivideo.event.ItemClickEvent
import com.ss.ivideo.model.VideoDetailContent
import com.ss.ivideo.model.VideoItemModel
import com.ss.ivideo.model.VideoPlayListItem
import com.ss.ivideo.utils.SharedPreferencesUtils
import kotlinx.android.synthetic.main.fragment_detail.*
import kotlinx.android.synthetic.main.item_detail_header.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe


/**
 * Created by liushuai on 2019-10-30
 */
class DetailFragment : Fragment() {

    var url: String? = null
        set(value) {
            field = "${Constant.URL_BASE}$value"
            //webViewEngine?.url = field
        }

    var title: String? = ""

    var webViewEngine: WebViewEngine? = null

    var videoDetailContent: VideoDetailContent? = null
    val orientationUtils: OrientationUtils by lazy { OrientationUtils(activity, video_player) }
    val gsyVideoOption = GSYVideoOptionBuilder()
    var videoUrls: Array<String>? = null
    var dialog: MaterialDialog? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_detail, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        EventBus.getDefault().register(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
    }

    override fun onDestroyView() {
        if (webview != null) {
            webview.stopLoading()
            webview.removeAllViews()
            webview.destroy()
        }
        video_player?.release()
        super.onDestroyView()
    }

    override fun onPause() {
        super.onPause()
        video_player.currentPlayer.onVideoPause()
    }

    override fun onResume() {
        super.onResume()
        video_player.currentPlayer.onVideoResume()

    }


    private fun initView() {
        webViewEngine = WebViewEngine(webview, object : WebViewEngine.LoadCallback {
            override fun onVideoUrlLoad(url: String?, videoUrl: Array<String>?) {
                if (videoUrls.isNullOrEmpty()) {
                    videoUrls = videoUrl
                    initVideoData(videoUrl?.get(0))
                }
            }

            override fun onHtmlLoad(url: String?, html: String?) {
                updateContentView(html)
            }

        }).apply {
            this.url = this@DetailFragment.url
        }
        cover.visibility = View.VISIBLE

        //增加banner广告
        //TTAdManagerHolder.addBanner(context, bannerContainer)
        AdMobManager.addBanner(bannerContainer, Constant.APP_DETAIL_BANNER_ID)

        video_player.apply {
            backButton.setOnClickListener {
                activity?.finish()
            }
            fullscreenButton.setOnClickListener {
                //直接横屏
                orientationUtils.resolveByClick()
                //第一个true是否需要隐藏actionbar，第二个true是否需要隐藏statusbar
                startWindowFullscreen(
                    context,
                    true,
                    true
                )
            }
        }
    }

    private fun initVideoPlayer(url: String?) {

        gsyVideoOption
            .setIsTouchWiget(true)
            .setRotateViewAuto(false)
            .setLockLand(false)
            .setAutoFullWithSize(true)
            //.setUrl(url)
            .setShowFullAnimation(false)
            .setNeedLockFull(true)
            .setOverrideExtension("m3u8")
            .setCacheWithPlay(false)
            .setVideoAllCallBack(object : GSYSampleCallBack() {
                override fun onPrepared(url: String?, vararg objects: Any?) {
                    super.onPrepared(url, *objects)
                    //开始播放了才能旋转和全屏
                    orientationUtils.isEnable = true
                }

                override fun onQuitFullscreen(url: String?, vararg objects: Any?) {
                    super.onQuitFullscreen(url, *objects)
                    if (orientationUtils != null) {
                        orientationUtils.backToProtVideo();
                    }
                }
            }).setLockClickListener { view, lock ->
                if (orientationUtils != null) {
                    //配合下方的onConfigurationChanged
                    orientationUtils.isEnable = !lock
                }
            }.setStartAfterPrepared(true)
            .build(video_player)

        val urls = ArrayList<GSYSampleADVideoPlayer.GSYADVideoModel>()
        //广告1
//        urls.add(GSYSampleADVideoPlayer.GSYADVideoModel("http://7xjmzj.com1.z0.glb.clouddn.com/20171026175005_JObCxCE2.mp4", "", GSYSampleADVideoPlayer.GSYADVideoModel.TYPE_AD, true))
        //正式内容1
        urls.add(
            GSYSampleADVideoPlayer.GSYADVideoModel(
                url,
                title,
                GSYSampleADVideoPlayer.GSYADVideoModel.TYPE_NORMAL
            )
        )
        video_player.setAdUp(urls, true, 0)

        video_player.startPlayLogic()
    }


    private fun initVideoData(m3u8Url: String?) {
        playVideo(m3u8Url)
    }

    private fun playVideo(videoUrl: String?) {
        Log.e("play_url = ", "$videoUrl")
        Handler(Looper.getMainLooper()).post {
            cover.visibility = View.GONE
            //GSYVideoManager.releaseAllVideos()
            initVideoPlayer(videoUrl)
        }
    }

    private fun updateContentView(html: String?) {
        if (videoDetailContent == null) {
            videoDetailContent = HtmlParseUtils.parseDetailContent(html)
            Handler(Looper.getMainLooper()).post {
                updateContent()
            }
        }
    }

    private fun updateContent() {
        val items = videoDetailContent?.playListItem
        items ?: return
        items.forEachIndexed { index, videoPlayListItem ->
            if (index == 0) {
                videoPlayListItem.isPlay = "on"
            }
            videoPlayListItem.url = videoUrls?.getOrNull(index)
        }
        recycler_view_recommend.layoutManager = GridLayoutManager(context, 3)
        recycler_view_recommend.adapter = VideoListAdapter().apply {
            setNewData(videoDetailContent?.recommend)
            setOnItemClickListener { adapter, view, position ->
                val cxt = context
                cxt ?: return@setOnItemClickListener
                adapter.data.getOrNull(position)?.let { it ->
                    (it as? VideoItemModel)?.let { item ->
                        DetailActivity.open(cxt, item.url, item.title)
                    }
                }
            }
        }

        initHeader()
        //初始化广告
        initAd(video_player) {
            video_player.startPlayLogic()
        }
    }

    private fun initHeader() {
        val recommendAdapter = recycler_view_recommend.adapter as VideoListAdapter
        recommendAdapter.removeAllHeaderView()
        val headerView = LayoutInflater.from(context).inflate(R.layout.item_detail_header, null)
        recommendAdapter.addHeaderView(headerView)
        val recycler_view = headerView.findViewById<RecyclerView>(R.id.recycler_view)
        recycler_view.layoutManager = LinearLayoutManager(context, HORIZONTAL, false)
        recycler_view.adapter = PlayListAdapter().apply {
            setOnItemClickListener { adapter, view, position ->
                adapter.data.forEach {
                    (it as VideoPlayListItem).isPlay = ""
                }
                (adapter.data.getOrNull(position) as VideoPlayListItem).let {
                    playVideo(it.url)
                    it.isPlay = "on"
                }
                adapter.notifyDataSetChanged()
            }
            setNewData(videoDetailContent?.playListItem)
        }

        val vTitle = headerView.findViewById<TextView>(R.id.vTitle)
        vTitle.text = videoDetailContent?.title
        val subTitle = headerView.findViewById<TextView>(R.id.subTitle)
        subTitle.text = videoDetailContent?.subTitle
        val desc = headerView.findViewById<TextView>(R.id.desc)
        desc.text = videoDetailContent?.desc
        val itemAll = headerView.findViewById<TextView>(R.id.item_all)
        if (getPlayItemsCount() > 5) {
            itemAll.visibility = View.VISIBLE
        } else {
            itemAll.visibility = View.GONE
        }
        itemAll.setOnClickListener {
            DetailPlayListDialogFragment.open(videoDetailContent?.playListItem, childFragmentManager)
        }

        //hide loading
        video_loading.visibility = View.GONE
    }

    private fun getPlayItemsCount(): Int {
        return videoDetailContent?.playListItem?.size ?: 0
    }

    private fun showPlayListItemsDialog() {
//        BottomSheetDialogFragment
    }

    private fun initAd(videoPlayer: GSYSampleADVideoPlayer, callback: () -> Unit) {
        if ((videoDetailContent?.sensitive == true && !SharedPreferencesUtils.sensitivePermission)  || SharedPreferencesUtils.coin == 0) {
            AdMobManager.addRewardedAd(activity!!) { success, rewardAd ->
                if (success) {
                    val msg = if (videoDetailContent?.sensitive == true) {
                        R.string.dialog_title_sensitive_msg
                    } else {
                        R.string.dialog_title_tips_msg
                    }

                    dialog = MaterialDialog(context!!).show {
                        cornerRadius(16f)
                        title(R.string.dialog_title_alert)
                        message(msg)
                        positiveButton {
                            AdMobManager.showRewardAd(activity!!, rewardAd) { watched, count ->
                                if (watched) {
                                    if (videoDetailContent?.sensitive == false) {
                                        SharedPreferencesUtils.coin =
                                            SharedPreferencesUtils.coin + count
                                    } else {
                                        SharedPreferencesUtils.sensitivePermission = true
                                    }
                                    callback()
                                } else {
                                    initAd(videoPlayer, callback)
                                }
                            }
                            dismiss()
                        }

                        negativeButton {
                            dismiss()
                            activity?.finish()
                        }
                        Handler(Looper.getMainLooper()).post {
                            if (videoPlayer.currentPlayer.isInPlayingState) {
                                videoPlayer.currentPlayer.onVideoPause()
                            } else {
                                videoPlayer.currentPlayer.release()
                            }
                        }
                    }.apply {
                        noAutoDismiss()
                        cancelOnTouchOutside(false)
                        setOnKeyListener { dialog, keyCode, event ->

                            if (keyCode == KeyEvent.KEYCODE_BACK) {
                                true
                            } else {
                                true
                            }
                        }
                    }
                }
            }
        }

        if (videoDetailContent?.sensitive == false && SharedPreferencesUtils.coin > 0) {
            SharedPreferencesUtils.coin = SharedPreferencesUtils.coin - 1
        }

    }

    fun onbackPress(): Boolean {
        return dialog?.isShowing == true
    }

    @Subscribe
    fun onDialogItemClick(itemClickEvent: ItemClickEvent){
        (recycler_view.layoutManager as LinearLayoutManager).scrollToPositionWithOffset(itemClickEvent.position, 50)
        recycler_view.adapter?.notifyDataSetChanged()
        ((recycler_view.adapter as? PlayListAdapter)?.data?.getOrNull(itemClickEvent.position))?.let {
            playVideo(it.url)
        }
    }

}

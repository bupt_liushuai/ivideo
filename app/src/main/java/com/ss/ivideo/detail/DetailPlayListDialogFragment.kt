package com.ss.ivideo.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.ss.ivideo.R
import com.ss.ivideo.event.ItemClickEvent
import com.ss.ivideo.model.VideoPlayListItem
import kotlinx.android.synthetic.main.fragment_video_detail_play_list.*
import org.greenrobot.eventbus.EventBus

/**
 * Created by liushuai on 2019-12-03
 */
class DetailPlayListDialogFragment: BottomSheetDialogFragment() {

    var playListItem: List<VideoPlayListItem>? = null
    companion object {
        fun open(playListItem: List<VideoPlayListItem>? = null, fragmentManager: FragmentManager) {
            val f = DetailPlayListDialogFragment()
            f.playListItem = playListItem
            f.show(fragmentManager, "DetailPlayListDialogFragment")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_video_detail_play_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    fun initView() {
        recycler_view.layoutManager = GridLayoutManager(context, 4)
        recycler_view.adapter = PlayListAdapter(R.layout.item_dialog_play_list).apply {
            setOnItemClickListener { adapter, view, position ->
                adapter.data.forEach {
                    (it as VideoPlayListItem).isPlay = ""
                }
                (adapter.data.getOrNull(position) as VideoPlayListItem).let {
                    it.isPlay = "on"
                }
                adapter.notifyDataSetChanged()
                this@DetailPlayListDialogFragment.dismiss()
                EventBus.getDefault().post(ItemClickEvent(position))
            }
            setNewData(playListItem)
        }
    }

}
package com.ss.ivideo.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.ss.ivideo.R

/**
 * Created by liushuai on 2019-11-22
 */
class CategorySelector @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
        LinearLayout(context, attrs, defStyleAttr) {

    lateinit var recyclerView1: RecyclerView
    lateinit var recyclerView2: RecyclerView
    lateinit var recyclerView3: RecyclerView
    lateinit var recyclerView4: RecyclerView
    var type0: Pair<String, String>? = null
    var type1: Pair<String, String>? = null
    var type2: Pair<String, String>? = null
    var type3: Pair<String, String>? = null

    var list = mutableListOf<List<Pair<String, String>>>() // category list
        set(value) {
            field = value
            updateCategory()
        }

    init {
        initView()
    }

    var selectCallback: ((String, String, String, String) -> Unit)? = null

    fun setCallback(callback: ((String, String, String, String) -> Unit)) {
        selectCallback = callback
    }

    private fun initView() {
        LayoutInflater.from(context).inflate(R.layout.item_category_view, this)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        recyclerView1 = findViewById(R.id.category_1)
        recyclerView2 = findViewById(R.id.category_2)
        recyclerView3 = findViewById(R.id.category_3)
        recyclerView4 = findViewById(R.id.category_4)
    }

    private fun updateCategory() {
        list.add(mutableListOf(Pair("最新", "newstime"), Pair("最热", "onclick")))

        list.getOrNull(0)?.let {
            updateRecycler(recyclerView1, it, TYPE_0)
        }?: let {
            recyclerView1.visibility = View.GONE
        }

        list.getOrNull(1)?.let {
            updateRecycler(recyclerView2, it, TYPE_1)
        }?: let {
            recyclerView2.visibility = View.GONE
        }

        list.getOrNull(2)?.let {
            updateRecycler(recyclerView3, it, TYPE_2)
        }?: let {
            recyclerView3.visibility = View.GONE
        }

        list.getOrNull(3)?.let {
            updateRecycler(recyclerView4, it, TYPE_3)
        }?: let {
            recyclerView4.visibility = View.GONE
        }
    }

    private fun updateRecycler(recyclerView: RecyclerView, date:List<Pair<String, String>>, type: Int) {
        recyclerView.apply {
            visibility = View.VISIBLE
            layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
            adapter = CategorySelectorAdapter().apply {
                setNewData(date)
                setOnItemClickListener { adapter, view, position ->
                    val data = adapter.data.getOrNull(position) as Pair<String, String>
                    when (type) {
                        TYPE_0 -> type0 = data
                        TYPE_1 -> type1 = data
                        TYPE_2 -> type2 = data
                        TYPE_3 -> type3 = data
                    }

                    selectCallback?.invoke(type0?.second?:"", type1?.second?:"", type2?.second?:"", type3?.second?:"")
                    list = mutableListOf(type0?.first?:"", type1?.first?:"", type2?.first?:"", type3?.first?:"")
                }
            }
        }
    }

    companion object {
        const val TYPE_0 = 0
        const val TYPE_1 = 1
        const val TYPE_2 = 2
        const val TYPE_3 = 3
    }

}

class CategorySelectorAdapter(): BaseQuickAdapter<Pair<String, String>, BaseViewHolder>(R.layout.item_category_header_item) {
    var list = mutableListOf<String>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    override fun convert(helper: BaseViewHolder, item: Pair<String, String>?) {
        item?: return
        helper.setText(R.id.title, item.first)
        helper.getView<TextView>(R.id.title).isSelected = list.contains(item.first)
    }

}


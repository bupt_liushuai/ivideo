package com.ss.ivideo.core

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.bumptech.glide.request.RequestOptions
import com.ss.ivideo.R


/**
 * Created by liushuai on 2019-11-02
 */
object GlideUtils {
    fun loadImage(imageView: ImageView, url: String) {
        Glide.with(imageView).load(url).into(imageView)
    }

    fun loadImage(imageView: ImageView, url: String, corner: Int) {
        Glide.with(imageView)
                .load(url)
                .transition(withCrossFade())
                .skipMemoryCache(false)
                .apply(RequestOptions.bitmapTransform(RoundedCorners(corner)))
                .placeholder(R.mipmap.ic_placeholder)
                .into(imageView)
    }
}
package com.ss.ivideo.core

import android.annotation.SuppressLint
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.text.TextUtils
import android.webkit.JavascriptInterface
import android.webkit.WebView

/**
 * Created by liushuai on 2019-11-01
 */
class WebViewEngine(var webview: WebView, var callback: LoadCallback) {

    init {
        configWebView()
    }

    var url: String? = null
        set(value) {
            field = value
            if (!TextUtils.isEmpty(url)) {
                webview.loadUrl(url)
                handler.sendEmptyMessageDelayed(1, 50)
            }
        }

    var handler = object : Handler(Looper.getMainLooper()) {
        override fun handleMessage(msg: Message) {
            super.handleMessage(msg)
            loadJS()
        }
    }

    private fun loadJS() {
        webview.loadUrl("javascript:window.java_obj.showUrls(urls)")
        handler.sendEmptyMessageDelayed(1, 200)
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun configWebView() {
        webview.apply {
            settings.javaScriptEnabled = true
            addJavascriptInterface(InJavaScriptLocalObj(callback), "java_obj")
        }
    }

    interface LoadCallback {
        fun onVideoUrlLoad(url: String?, videoUrls: Array<String>?)
        fun onHtmlLoad(url: String?, html: String?)
    }

    inner class InJavaScriptLocalObj(var callback: LoadCallback) {
        @JavascriptInterface
        fun showSource(url: String, html: String) {
            callback.onHtmlLoad(url, html)
            println("webview urls = showsource")
            handler.post {
                webview?.stopLoading()
                webview?.loadUrl("")
            }
        }

        @JavascriptInterface
        fun showUrls(urls: Array<String>?) {
            if (urls != null && urls.isNotEmpty()) {
                println("webview urls = ${urls[0]}")
                callback.onVideoUrlLoad(url, urls)
                handler.removeMessages(1)
                handler.post {
                    webview?.loadUrl("javascript:window.java_obj.showSource(" + "'${url}'," + "document.getElementsByTagName('html')[0].innerHTML);")
                }
            }
        }
    }

}
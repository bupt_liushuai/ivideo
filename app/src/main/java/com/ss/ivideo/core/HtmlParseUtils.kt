package com.ss.ivideo.core

import com.ss.ivideo.model.*
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element

/**
 * Created by liushuai on 2019-11-01
 */
object HtmlParseUtils {

    fun parseHomeList(document: Document): List<VideoSectionData> {
        val result = mutableListOf<VideoSectionData>()
        val elements = document.getElementsByClass("lists lists-thumb-top lists-title-lines-1")
        elements.forEachIndexed { index, element ->
            val sectionTitle = element.select("h3")[0].text()
            val listContent = element.getElementsByClass("lists-content")
            val liElements = if (listContent.size > 1) {
                listContent[0].getElementsByTag("li")
            } else {
                listContent[0].getElementsByTag("li")
            }
            var sectionData: VideoSectionData = VideoSectionData().apply {
                title = sectionTitle
            }
            result.add(sectionData)
            sectionData.list = mutableListOf()
            liElements.forEach {
                val item = parseVideoItem(it)
                sectionData.list.add(item)
            }
        }
        return result
    }

    private fun parseVideoItem(element: Element): VideoItemModel {
        val item = VideoItemModel()
        val href = element.select("a[class=thumbnail]")[0].attr("href")
        item.url = href
        val img = element.select("a > img")[0].attr("src")
        item.thumb = img
        val title = element.select("a > img")[0].attr("alt")
        item.title = title
        val note = element.select("a > div > span")[0].text()
        item.note = note
        return item
    }


    //解析分类列表
    fun parseCategoryList(document: Document): VideoCategoryModel? {
        //解析列表
        val lis = document.select("div[class=lists-content] > ul > li")
        val items = mutableListOf<VideoItemModel>()
        lis.forEach {
            val item = parseVideoItem(it)
            items.add(item)
        }

        //解析分类。
        val categoryDivs = document.select("div[class=lists-content filter] > dl")
        val cagegoryMap = HashMap<Int, List<Pair<String, String>>>()
        categoryDivs.forEachIndexed { index, element ->
            val list = mutableListOf<Pair<String, String>>()
            cagegoryMap[index] = list
            element.select("dd > a")?.forEachIndexed { index, itemElement ->

                val name = itemElement.text()
                val value = if (index == 0) {
                    ""
                } else {
                    itemElement.text()
                }
                val pair = Pair<String, String>(name, value)
                list.add(pair)

            }
        }

        return VideoCategoryModel().apply {
            this.itemModels = items
            this.category = cagegoryMap
        }
    }


    //解析播放详情， 视频信息+播放列表
    fun parseDetailContent(html: String?): VideoDetailContent? {
        html ?: return null
        val document = Jsoup.parse(html)
        val itemList = mutableListOf<VideoPlayListItem>()

        //解析列表
        val lis = document.select("div[class=playlist clearfix] > ul > li")
        lis.forEach {
            val content = it.select("a")[0].text()
            val js = it.select("a")[0].attr("onclick")
            val item = VideoPlayListItem(content, js)
            itemList.add(item)
        }
        //解析内容
        val title = document.select("h1[class=product-title]")[0].text()

        val excerpt = document.select("div[class=product-excerpt]")

        var region = ""
        var category = ""
        var desc = ""
        excerpt.forEach {
            when {
                it.text().contains("地区") -> region = it.select("span > a")[0].text()
                it.text().contains("类型") -> category = it.select("span")[0].text()
                it.text().contains("剧情简介") -> desc = it.select("span")[0].text()
            }
        }
        val subTitle = "$region / $category"

        //底部推荐内容
        val list = document.select("div[class=lists-content] > ul > li")
        val recommend = mutableListOf<VideoItemModel>()
        if (!list.isNullOrEmpty()) {
            list.forEach {
                parseRecommendVideoItem(it)?.let {
                    recommend.add(it)
                }
            }
        }

        return VideoDetailContent().apply {
            this.playListItem = itemList
            this.title = title
            this.subTitle = subTitle
            this.desc = desc
            this.recommend = recommend
            this.sensitive = category.contains("情色")
        }
    }

    private fun parseRecommendVideoItem(element: Element): VideoItemModel {
        val item = VideoItemModel()
        val href = element.select("a[class=thumbnail]")[0].attr("href")
        item.url = href
        val img = element.select("a > img")[0].attr("src")
        item.thumb = img
        val title = element.select("h2 > a")[0].text()
        item.title = title
        return item
    }

}
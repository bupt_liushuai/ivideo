package com.ss.ivideo.core

import com.ss.ivideo.model.VideoCategoryModel
import com.ss.ivideo.model.VideoItemModel
import com.ss.ivideo.model.VideoSectionData
import io.reactivex.Observable
import org.jsoup.Jsoup

/**
 * Created by liushuai on 2019-11-04
 */
object HtmlDataLoader {

    fun getHomeListAsync(url: String): Observable<List<VideoSectionData>> {
        return Observable.fromCallable {
            val document = Jsoup.connect(url).get()
            val data = HtmlParseUtils.parseHomeList(document)
            data
        }
    }

    fun getCategoryListAsync(url: String): Observable<VideoCategoryModel> {
        return Observable.fromCallable {
            val document = Jsoup.connect(url).get()
            HtmlParseUtils.parseCategoryList(document)
        }
    }
}
package com.ss.ivideo.utils

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.ss.ivideo.app.BaseApplication


/**
 * Created by Liushuai on 2019/11/19.
 */
object Utils {
    fun hideInput(view: View) {
        val imm = BaseApplication.instance.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?

        imm!!.hideSoftInputFromWindow(view.windowToken, 0) //强制隐藏键盘
    }

    fun showInput(view: View) {
        val imm = BaseApplication.instance.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.showSoftInput(view, InputMethodManager.SHOW_FORCED)
    }
}
package com.ss.ivideo.utils;


import android.text.TextUtils;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;

import java.io.Reader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class GsonUtil {

    private static final Gson gson = new Gson();

    private GsonUtil() {}

    /**
     * 将object对象转成json字符串
     *
     * @param object
     * @return
     */
    public static String toJson(Object object) {
        String gsonString = null;
        gsonString = gson.toJson(object);
        return gsonString;
    }

    /**
     * Convert gson file to bean
     *
     * @param reader
     * @param cls
     * @return
     */
    public static <T> T fileToBean(Reader reader, Class<T> cls) {
        T t = null;
        try {
            t = gson.fromJson(reader, cls);
        } catch (JsonSyntaxException e){
            e.printStackTrace();
        }
        return t;
    }


    /**
     * 将gsonString转成泛型bean
     *
     * @param gsonString
     * @param cls
     * @return
     */
    public static <T> T gsonToBean(String gsonString, Class<T> cls) {
        T t = null;
        try {
            t = gson.fromJson(gsonString, cls);
        } catch (JsonSyntaxException e){
            e.printStackTrace();
        }
        return t;
    }

    /**
     * 将gsonString转成泛型bean
     *
     * @param gsonString
     * @param type
     * @return
     */
    public static <T> T gsonToBean(String gsonString, Type type) {
        T t = null;
        try {
            t = gson.fromJson(gsonString, type);
        } catch (JsonSyntaxException e){
            e.printStackTrace();
        }
        return t;
    }

    /**
     * 转成list
     * 解决泛型问题
     *
     * @param json
     * @param cls
     * @param <T>
     * @return
     */
    public static <T> List<T> jsonToList(String json, Class<T> cls) {
        List<T> list = new ArrayList<T>();
        if (!TextUtils.isEmpty(json)) {
            JsonArray array = new JsonParser().parse(json).getAsJsonArray();
            try {
                for (final JsonElement elem : array) {
                    list.add(gson.fromJson(elem, cls));
                }
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
        return list;
    }


    /**
     * 转成list中有map的
     *
     * @param gsonString
     * @return
     */
    public static <T> List<Map<String, T>> gsonToListMaps(String gsonString) {
        List<Map<String, T>> list = null;
        try {
            list = gson.fromJson(gsonString,
                    new TypeToken<List<Map<String, T>>>() {
                    }.getType());
        } catch (JsonSyntaxException e){
            e.printStackTrace();
        }
        return list;
    }


    /**
     * 转成map的
     *
     * @param gsonString
     * @return
     */
    public static <T> Map<String, T> gsonToMaps(String gsonString) {
        Map<String, T> map = null;
        try {
            map = gson.fromJson(gsonString, new TypeToken<Map<String, T>>() {}.getType());
        } catch (JsonSyntaxException e){
            e.printStackTrace();
        }
        return map;
    }
}
package com.ss.ivideo.app

import androidx.multidex.MultiDexApplication
import kotlin.properties.Delegates


/**
 * Created by liushuai on 2019-11-07
 */
class BaseApplication : MultiDexApplication() {
    companion object {
        var instance: BaseApplication by Delegates.notNull()
    }
    override fun onCreate() {
        super.onCreate()
        init()
    }

    private fun init() {
        instance = this
    }

}
package com.ss.ivideo.base

import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.jaeger.library.StatusBarUtil
import com.ss.ivideo.R
import androidx.core.graphics.ColorUtils
import androidx.annotation.ColorInt



/**
 * Created by liushuai on 2019-11-02
 */
abstract class BaseActivity: AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStatusBarColor(R.color.actionbarColor)
        setActionBarColor(R.color.actionbarColor)
    }

    fun setStatusBarColor(color: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            StatusBarUtil.setColor(this, resources.getColor(color), 0)
            if (isLightColor(resources.getColor(color))) {
                window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            } else {
                window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE;
            }
        } else {
            //加透明度
            StatusBarUtil.setColor(this, resources.getColor(color))
        }
    }

    open fun setActionBarColor(color: Int) {
        supportActionBar?.apply {
            setBackgroundDrawable(ColorDrawable(resources.getColor(color)))
            elevation = 0f
        }
    }

    private fun isLightColor(@ColorInt color: Int): Boolean {
        return ColorUtils.calculateLuminance(color) >= 0.5
    }

}
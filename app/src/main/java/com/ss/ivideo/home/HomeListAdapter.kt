package com.ss.ivideo.home

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.ss.ivideo.R
import com.ss.ivideo.cagegory.VideoListAdapter
import com.ss.ivideo.detail.DetailActivity
import com.ss.ivideo.model.VideoSectionData

/**
 * Created by liushuai on 2019-11-01
 */
class HomeListAdapter: BaseQuickAdapter<VideoSectionData, BaseViewHolder>(R.layout.item_home_list) {
    override fun convert(helper: BaseViewHolder, item: VideoSectionData?) {
        item ?: return
        helper.getView<RecyclerView>(R.id.recycler_view).apply {
            //GlideUtils.loadImage(this, item.thumb)
            layoutManager = GridLayoutManager(context, 3)
            adapter = VideoListAdapter().apply {
                val list = if (item.list.size > 6) {
                    item.list.subList(0, 6)
                } else {
                    item.list
                }
                setNewData(list)
                setOnItemClickListener { _, _, position ->
                    DetailActivity.open(context, list.getOrNull(position)?.url, list.getOrNull(position)?.title)
                }
            }
        }
        helper.setText(R.id.title, item.title)
    }
}
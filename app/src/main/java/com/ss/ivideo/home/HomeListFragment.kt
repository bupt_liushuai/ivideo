package com.ss.ivideo.home

import android.os.Bundle
import android.text.TextUtils
import android.util.SparseArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.ss.ivideo.R
import com.ss.ivideo.api.IApiService
import com.ss.ivideo.api.RetrofitHelper
import com.ss.ivideo.cagegory.CategoryListActivity
import com.ss.ivideo.constant.Constant
import com.ss.ivideo.core.HtmlDataLoader
import com.ss.ivideo.core.HtmlParseUtils
import com.ss.ivideo.detail.DetailActivity
import com.ss.ivideo.model.VideoItemModel
import com.ss.ivideo.model.VideoSectionData
import com.ss.ivideo.utils.GsonUtil
import com.ss.ivideo.utils.SharedPreferencesUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_home.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by liushuai on 2019-10-30
 */

class HomeListFragment : Fragment() {

    private var list = mutableListOf<VideoSectionData>()
    var adapter: HomeListAdapter? = null
    var disposable: Disposable? = null
    var headerView: View? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onDestroy() {
        if (disposable?.isDisposed == false) {
            disposable?.dispose()
        }
        super.onDestroy()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        EventBus.getDefault().post(LoadHtmlEvent(Constant.URL_DY))
        initView()
        loadData()
    }

    private fun initView() {
        adapter = HomeListAdapter().apply {
            setOnItemClickListener { adapter, view, position ->
                val cxt = context
                cxt ?: return@setOnItemClickListener
                list.getOrNull(position)?.let {
                    //DetailActivity.open(cxt, it.url)
                }

            }
        }
        recycler_view.adapter = adapter
        recycler_view.layoutManager = LinearLayoutManager(context)
        adapter?.setNewData(list)

        initHeaderView()
        loading_view.visibility = View.VISIBLE
    }

    private fun initHeaderView() {
        headerView = LayoutInflater.from(context).inflate(R.layout.item_home_header, null)
        adapter?.removeAllHeaderView()
        adapter?.addHeaderView(headerView)
        headerView?.findViewById<View>(R.id.btn1)?.setOnClickListener {
            CategoryListActivity.open(context, Constant.URL_DY, "电影")
        }
        headerView?.findViewById<View>(R.id.btn2)?.setOnClickListener {
            CategoryListActivity.open(context, Constant.URL_DSJ, "电视剧")

        }
        headerView?.findViewById<View>(R.id.btn3)?.setOnClickListener {
            CategoryListActivity.open(context, Constant.URL_ZY, "综艺")

        }
        headerView?.findViewById<View>(R.id.btn4)?.setOnClickListener {
            CategoryListActivity.open(context, Constant.URL_DM,"动漫")
        }
    }

    private fun loadData() {
        initData()
        disposable = HtmlDataLoader.getHomeListAsync(Constant.URL_BASE).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    bindData(it)
                    saveData(it)
                    loading_view.visibility = View.GONE
                }, {
                    println("error")
                    loading_view.visibility = View.GONE
                })
    }

    private fun initData() {
        val localData = SharedPreferencesUtils.homePageData
        if (!TextUtils.isEmpty(localData)) {
            val list = GsonUtil.jsonToList(localData, VideoSectionData::class.java)
            if (list!= null && list.isNotEmpty()) {
                bindData(list)
            }
        }
    }

    private fun bindData(list: List<VideoSectionData>) {
        adapter?.setNewData(list)
    }

    private fun saveData(list: List<VideoSectionData>) {
        SharedPreferencesUtils.homePageData = GsonUtil.toJson(list)
    }
}